var uuid = require("uuid");
var express = require("express");
var app = express();
var emails = { };

app.use(
    express.static(__dirname + "/public")
);

app.use(
    express.static(__dirname + "/bower_components")
);

app.use("/index.html", function (req, res) {
    res.status(200);
    res.type("text/html");
});

app.get("/reg", function (req, res) {
    var email = req.query.email;
    var password = req.query.password;
    var name = req.query.name;
    var gender = req.query.gender;
    var birthdate = req.query.birthdate;
    var address = req.query.address;
    var country = req.query.country;
    var contact = req.query.contact;

    console.log("Email: %s, Password: %s, Name: %s, Gender: %s, Date of Birth: %s, Address: %s, Country: %s, Contact number: %s", req.query.email, req.query.password, req.query.name, req.query.gender, req.query.birthdate, req.query.address, req.query.country, req.query.contactno);

    // if email exists
    if (emails[email]) {
        res.status(400);
        res.send();
        return;
    }

    var regid = uuid.v1().substring(0, 8);
    emails[email] = regid;

    res.status(202);
    res.type("application/json");
    res.json({ regid: regid });
});

port = 3000;
if (process.argv[2]) {
    port = +process.argv[2];
}

app.listen(
    port,
    function () {
        console.log("application started on port %d", port);
    }
);