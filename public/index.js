(function () {
    var RegApp = angular.module("RegApp", []);

    var RegCtrl = function ($http) {
        var regCtrl = this;

        regCtrl.email = "";
        regCtrl.password = "";
        regCtrl.name = "";
        regCtrl.gender = "";
        regCtrl.birthdate = "";
        regCtrl.address = "";
        regCtrl.country = "";
        regCtrl.contactno = "";

        regCtrl.msg = "";
        regCtrl.age = "";

        regCtrl.submit = function () {

            console.log("Email: %s, Password: %s, Name: %s, Gender: %s, Birthdate: %s, Address: %s, Country: %s, Contact no.: %s", regCtrl.email, regCtrl.password, regCtrl.name, regCtrl.gender, regCtrl.birthdate, regCtrl.address, regCtrl.country, regCtrl.contactno);

            $http.get("/reg", {
                params: {
                    email: regCtrl.email,
                    password: regCtrl.password,
                    name: regCtrl.name,
                    gender: regCtrl.gender,
                    birthdate: regCtrl.birthdate,
                    address: regCtrl.address,
                    country: regCtrl.country,
                    contactno: regCtrl.contactno
                }
            }).then(function (result) { // 200 response
                regCtrl.msg = "Thank you. You have successfully registered!";
            }).catch(function () { // 400, 500 response
                regCtrl.msg = regCtrl.email + " has already been registered.";
            })
        }

        // isValid validation
        regCtrl.isValid = function (regForm, fieldName) {
            return (regForm[fieldName].$invalid && regForm[fieldName].$dirty);
        }

        // Age verification, must be at least 18 years old
        var today = new Date();
        var eighteen = 18;
        regCtrl.maxDate = new Date(today.getFullYear() - eighteen, today.getMonth(), today.getDate()); // birthdate must be less than 1999-06-22T16:00:00.000Z
    };

    RegCtrl.$inject = ["$http"];

    RegApp.controller("RegCtrl", RegCtrl);
})();